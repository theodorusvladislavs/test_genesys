<?php

namespace App\Http\Controllers;
use App\Models\Buy;
use App\Models\Sele;
use App\Models\Master;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['masters'] = Master::all();
        return view('home', $data);
    }

    public function index_sale()
    {
        $data['masters'] = Master::all();
        return view('sale', $data);
    }

    public function datamaster()
    {
        $data['masters'] = Master::all();
        return view('master', $data);
    }

    public function buy(Request $request)
    {
        
        $request->validate([
            'produk' => 'required',
            'jumlah' => 'required|numeric'
        ]);

        $master = Master::find($request->produk);
        $master->stok += $request->jumlah;
        $master->save();

        $buy = new Buy;
        $buy->master_id = $request->produk;
        $buy->jumlah = $request->jumlah;
        $buy->save();

        $data['jumlah'] = $request->jumlah;
        $data['nama'] = $master->nama;
        $data['harga'] = $master->harga;

        return view('slip-sale', $data);
    }

    public function sale(Request $request)
    {
        
        $request->validate([
            'produk' => 'required',
            'jumlah' => 'required|numeric'
        ]);

        $master = Master::find($request->produk);
        $master->stok -= $request->jumlah;
        $master->save();

        $sale = new Sele;
        $sale->master_id = $request->produk;
        $sale->jumlah = $request->jumlah;
        $sale->save();

        $data['jumlah'] = $request->jumlah;
        $data['nama'] = $master->nama;
        $data['harga'] = $master->harga;

        return view('slip-buy', $data);
    }

    public function deleteproduk($id)
    {
        Buy::where('master_id', $id)->delete();
        Sele::where('master_id', $id)->delete();
        Master::find($id)->delete();
        return redirect( route('datamaster') );
    }

    public function editproduk ($id)
    {
        $data['masters'] = Master::find($id);
        return view('edit-master', $data);
    }

    public function editpost(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'jumlah' => 'required|numeric'
        ]);

        if (!empty($request->id)) {
            $master = Master::find($request->id);
            $master->nama = $request->nama;
            $master->stok = $request->jumlah;
            $master->save();
        } else {
            $master = new Master;
            $master->nama = $request->nama;
            $master->stok = $request->jumlah;
            $master->save();
        }

        return redirect( route('datamaster') );

    }
}
