@extends('layouts.app')

@section('content')
<form action="{{ route('editpost') }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="id" value="{{ $masters->id }}" readonly>
    <div >
        Nama Barang
    </div>
    <div >
        <input type="text" name="nama" value="{{ $masters->nama }}" >
    </div>
    <div >
        Jumlah Barang
    </div>
    <div >
        <input type="text" value="{{ $masters->stok }}" name="jumlah">
    </div>
    <br/>
    <div >
        <input type="submit" name="submit" value="Submit">
    </div>
</form>


@endsection
