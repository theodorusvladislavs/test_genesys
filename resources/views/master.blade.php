@extends('layouts.app')

@section('content')
<table border="1" width="100%">
    <tr>
        <th>Nama</th>
        <th>Stok</th>
        <th>Harga Satuan</th>
        <th>Action</th>
    </tr>
    <tr>
        @foreach( $masters as $master)
            <td>{{ $master->nama }}</td>
            <td>{{ $master->stok }}</td>
            <td>{{ $master->harga }}</td>
            <td>
                <a href=" {{ route('deleteproduk',array($master->id)) }}">Delete<a> || <a href=" {{ route('editproduk', array($master->id) ) }}">Edit</a>
            </td>
        @endforeach
    </tr>
</table>
@endsection
