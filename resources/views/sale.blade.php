@extends('layouts.app')

@section('content')
<form action="{{ route('sale') }}" method="post">
{{ csrf_field() }}
    <div >
        Nama Barang
    </div>
    <div >
        <select name="produk">
            <option value="0">--- Select Produk ---</option>
            @foreach ($masters as $master)
                <option value="{{ $master->id }}">{{ $master->nama }}</option>
            @endforeach
        </select>
    </div>
    <div >
        Jumlah Barang
    </div>
    <div >
        <input type="text" name="jumlah">
    </div>
    <br/>
    <div >
        <input type="submit" name="submit" value="Submit">
    </div>
</form>


@endsection
