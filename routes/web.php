<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/sale', [App\Http\Controllers\HomeController::class, 'index_sale'])->name('sale');
Route::get('/datamaster', [App\Http\Controllers\HomeController::class, 'datamaster'])->name('datamaster');
Route::post('/buy', [App\Http\Controllers\HomeController::class, 'buy'])->name('buy');
Route::post('/sale', [App\Http\Controllers\HomeController::class, 'sale'])->name('salePost');

Route::get('deleteproduk/{id}', [App\Http\Controllers\HomeController::class, 'deleteproduk'])->name('deleteproduk');
Route::get('editproduk/{id}', [App\Http\Controllers\HomeController::class, 'editproduk'])->name('editproduk');
Route::post('/editpost', [App\Http\Controllers\HomeController::class, 'editpost'])->name('editpost');
